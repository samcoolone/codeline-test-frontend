import React from "react";
import moment from "moment";
import PropTypes from "prop-types";

const AllDaysWeather = ({ weathers }) => (
  <table className="table">
    <thead>
      <tr>
        <th>State</th>
        <th>Date</th>
        <th>Day</th>
        <th>Temp.</th>
        <th>Min Temp.</th>
        <th>Max Temp.</th>
      </tr>
    </thead>
    <tbody>
      {weathers.map(w => (
        <tr key={w.id}>
          <td>
            <img
              src={`https://www.metaweather.com/static/img/weather/png/64/${
                w.weather_state_abbr
              }.png`}
              alt="Weather Code"
            />
          </td>
          <td>{w.applicable_date}</td>
          <td>{moment(w.applicable_date).format("dddd")}</td>
          <td>{w.the_temp.toFixed(2)}</td>
          <td>{w.min_temp.toFixed(2)}</td>
          <td>{w.max_temp.toFixed(2)}</td>
        </tr>
      ))}
    </tbody>
  </table>
);

AllDaysWeather.propTypes = {
  weathers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      weather_state_abbr: PropTypes.string,
      applicable_date: PropTypes.string,
      the_temp: PropTypes.number,
      min_temp: PropTypes.number,
      max_temp: PropTypes.number
    })
  )
};

export default AllDaysWeather;
