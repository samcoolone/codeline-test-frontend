import React from "react";
import moment from "moment";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import AllDaysWeather from "./all-days-weather";

const getTodayWeather = consolidateWeather => {
  return (
    consolidateWeather.find(w =>
      moment(moment().format("YYYY-MM-DD")).isSame(w.applicable_date)
    ) || {}
  );
};

const Weather = ({ weather, showAllDays = false }) => {
  const todayTemperature = getTodayWeather(weather.consolidated_weather);
  return (
    <div className="card d-flex">
      <div className="card-body">
        <h3 className="card-title text-center">
          {weather.title}{" "}
          <img
            src={`https://www.metaweather.com/static/img/weather/png/64/${
              todayTemperature.weather_state_abbr
            }.png`}
            alt="Weather Code"
          />
        </h3>
        <h6>Temp. {(todayTemperature.the_temp || {}).toFixed(2)}</h6>
        <h6>Min Temp. {(todayTemperature.min_temp || {}).toFixed(2)}</h6>
        <h6>Max Temp. {(todayTemperature.max_temp || {}).toFixed(2)}</h6>
        {showAllDays ? (
          <div className="mt-5">
            <AllDaysWeather weathers={weather.consolidated_weather} />
            <Link to="/" className="btn btn-primary">
              Back
            </Link>
          </div>
        ) : (
          <Link to={`/weather/${weather.woeid}`} className="btn btn-primary">
            View Detail
          </Link>
        )}
      </div>
    </div>
  );
};

Weather.propTypes = {
  weather: PropTypes.shape({
    title: PropTypes.string.isRequired,
    consolidated_weather: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        weather_state_abbr: PropTypes.string,
        applicable_date: PropTypes.string,
        the_temp: PropTypes.number,
        min_temp: PropTypes.number,
        max_temp: PropTypes.number
      })
    )
  }).isRequired,
  showAllDays: PropTypes.bool
};

export default Weather;
