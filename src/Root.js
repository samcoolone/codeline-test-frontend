import React from "react";
import { Route, Link, withRouter } from "react-router-dom";
import Home from "./containers/home";
import Search from "./containers/search";
import Weather from "./containers/weather";

const Root = ({ match }) => (
  <div className="container">
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        Weather
      </Link>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <Link
            className={`nav-item nav-link ${
              match.path === "/" ? "active" : ""
            }`}
            to="/"
          >
            Home
          </Link>
        </div>
      </div>
    </nav>
    <Route path="/" exact component={Home} />
    <Route path="/search/:term" component={Search} />
    <Route
      path="/weather/:woeid"
      component={({ match }) => (
        <Weather woeid={+match.params.woeid} showAllDays />
      )}
    />
  </div>
);

export default withRouter(Root);
