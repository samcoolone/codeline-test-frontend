import { API_BASE_URI } from "./constants";
import Axios from "axios";

function createClient() {
  return Axios.create({
    baseURL: API_BASE_URI,
    timeout: 0,
    headers: { Accept: "application/json" }
  });
}

const client = createClient();

export default client;
