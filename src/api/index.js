import ApiClient from "./client";

export function getWeatherByWoeID(woeid, options = {}) {
  return ApiClient.get("", {
    params: {
      command: "location",
      woeid
    },
    ...options
  }).then(({ data }) => data);
}

export function getWeatherByName(keyword, options = {}) {
  return ApiClient.get("", {
    params: {
      command: "search",
      keyword
    },
    ...options
  })
    .then(
      ({ data }) =>
        data.length === 1 ? getWeatherByWoeID(data[0].woeid) : { data }
    )
    .then(data => data.data || data);
}
