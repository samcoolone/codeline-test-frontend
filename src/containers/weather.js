import React, { Component } from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { getWeatherByWoeID } from "../api";
import Loader from "../components/loader";
import Weather from "../components/weather";

class WeatherContainer extends Component {
  static propTypes = {
    woeid: PropTypes.number.isRequired
  };

  state = {
    loading: true,
    data: null,
    error: false
  };

  cancelToken;

  componentDidMount() {
    const woeid = this.props.woeid;
    this.cancelToken = axios.CancelToken.source();
    if (woeid) {
      return getWeatherByWoeID(woeid, {
        cancelToken: this.cancelToken.token
      }).then(data => {
        this.setState({ loading: false, data });
      });
    } else {
      return this.setState({ error: "No woeid found!" });
    }
  }

  componentWillUnmount() {
    this.cancelToken.cancel("Aborted");
  }

  render() {
    const { loading, error } = this.state;
    return loading || error ? (
      <div className="card d-flex justify-content-center align-items-center h-100">
        {error || <Loader />}
      </div>
    ) : (
      <Weather showAllDays={this.props.showAllDays} weather={this.state.data} />
    );
  }
}

export default WeatherContainer;
