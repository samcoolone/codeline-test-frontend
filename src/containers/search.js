import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { getWeatherByName } from "../api";
import Loader from "../components/loader";
import Weather from "../components/weather";

class Search extends Component {
  state = {
    loading: true,
    searchTerm: this.props.match.params.term,
    data: null
  };

  cancelToken;

  searchKeyWord(term) {
    this.cancelToken = axios.CancelToken.source();
    return getWeatherByName(term, {
      cancelToken: this.cancelToken.token
    });
  }

  componentWillUnmount() {
    this.cancelToken.cancel("Aborted");
  }

  componentDidMount() {
    if (this.state.searchTerm) {
      this.searchKeyWord(this.state.searchTerm).then(data => {
        this.setState({ loading: false, data });
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.match.params.term !== this.props.match.params.term) {
      this.setState({ loading: true });
      this.cancelToken.cancel("Aborted");
      this.searchKeyWord(this.props.match.params.term).then(data => {
        this.setState({ loading: false, data });
      });
    }
  }

  render() {
    const { searchTerm, loading, data } = this.state;
    return (
      <div className="row">
        <div className="col-12 mt-5">
          <form className="form-inline my-2 my-lg-0 d-flex">
            <input
              className="form-control mr-5 w-75"
              type="search"
              placeholder="Search"
              value={searchTerm}
              onChange={e => this.setState({ searchTerm: e.target.value })}
              aria-label="Search"
            />
            <Link
              className="btn btn-primary my-2 my-sm-0"
              to={`/search/${searchTerm}`}
            >
              Search
            </Link>
          </form>
        </div>
        <div className="col">
          {loading ? (
            <div className="h-100 d-flex mt-5 justify-content-center align-items-center">
              <Loader />
            </div>
          ) : (
            <div className="mt-5">
              {(Array.isArray(data) && !data.length) || !data ? (
                <h5 className="text-danger">
                  No Data found for {this.props.match.params.term}
                </h5>
              ) : (
                <div className="row">
                  {Array.isArray(data) &&
                    data.length > 0 &&
                    data.map(d => (
                      <div className="col-4 mt-5" key={d.woeid}>
                        <div className="card d-flex">
                          <div className="card-body text-center">
                            <h3 className="card-title text-center">
                              {d.title}
                            </h3>
                            <Link
                              to={`/weather/${d.woeid}`}
                              className="btn btn-primary"
                            >
                              View Detail
                            </Link>
                          </div>
                        </div>
                      </div>
                    ))}
                  {data.woeid && <Weather weather={this.state.data} />}
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Search;
