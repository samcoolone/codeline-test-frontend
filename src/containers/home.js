import React, { Component } from "react";
import { Link } from "react-router-dom";
import WeatherContainer from "./weather";

const InitialCities = {
  Istanbul: 2344116,
  Berlin: 638242,
  London: 44418,
  Helsinki: 565346,
  Dublin: 560743,
  Vancouver: 9807
};

class Home extends Component {
  state = {
    searchTerm: ""
  };

  render() {
    const { searchTerm } = this.state;
    return (
      <div className="row">
        <div className="col-12 mt-5">
          <form className="form-inline my-2 my-lg-0 d-flex">
            <input
              className="form-control mr-5 w-75"
              type="search"
              placeholder="Search"
              value={searchTerm}
              onChange={e => this.setState({ searchTerm: e.target.value })}
              aria-label="Search"
            />
            <Link
              className="btn btn-primary my-2 my-sm-0"
              to={`/search/${searchTerm}`}
            >
              Search
            </Link>
          </form>
        </div>
        {Object.keys(InitialCities).map(n => (
          <div className="col-4 mt-5" key={n}>
            <WeatherContainer woeid={InitialCities[n]} />
          </div>
        ))}
      </div>
    );
  }
}

export default Home;
